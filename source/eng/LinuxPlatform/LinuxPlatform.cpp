#include <eng/HAL/Platform.h>

#if ENG_LINUX
#include <cstdlib>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <physfs.h>
#include <portaudio.h>

static int32 wasGlfwInitialized{ 0 };
static int32 wasPhysFSInitialized{ 0 };
static int32 wasPortAudioInitialized{ 0 };
static int32 wasFreetypeInitialized{ 0 };

__attribute__((destructor))
static void ShutdownEngine( void )
{
    // Shutdown PortAudio.
    if( wasPortAudioInitialized )
    {
        Pa_Terminate( );
    }

    // Shutdown PhysicsFS.
    if( wasPhysFSInitialized )
    {
        PHYSFS_deinit( );
    }

    // Shutdown GLFW.
    if( wasGlfwInitialized )
    {
        glfwTerminate( );
    }
}

__attribute__((constructor))
static void InitializeEngine( void )
{
    // Initialize GLFW
    wasGlfwInitialized = glfwInit( );
    if( !wasGlfwInitialized )
    {
        std::exit( 1 );
    }

    // Create temporary window in order to initialize OpenGL 3.2

    glfwWindowHint( GLFW_CLIENT_API, GLFW_OPENGL_API );
    glfwWindowHint( GLFW_CONTEXT_CREATION_API, GLFW_NATIVE_CONTEXT_API );
    glfwWindowHint( GLFW_CONTEXT_VERSION_MAJOR, 3 );
    glfwWindowHint( GLFW_CONTEXT_VERSION_MINOR, 2 );
    glfwWindowHint( GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE );
    glfwWindowHint( GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE );

    GLFWwindow* tempWindow = glfwCreateWindow( 1, 1, "Temp Window", nullptr, nullptr );
    if( !tempWindow )
    {
        std::exit( 1 );
    }

    // Initialize glad.
    bool wasGladInitialized = gladLoadGLLoader( reinterpret_cast<GLADloadproc>( glfwGetProcAddress ) );
    if( !wasGladInitialized )
    {
        glfwDestroyWindow( tempWindow );
        // Failed to initialize glad - OpenGL 3.2 has not been initialized or is not supported.
        exit( 1 );
    }

    // Don't need the temporary window anymore. DESTROY IT!
    glfwDestroyWindow( tempWindow );
    tempWindow = nullptr;

    // Initialization succeeded. Was support for OpenGL 3.2 completed?
    if( !GLAD_GL_VERSION_3_2 )
    {
        // Nope.
        std::exit( 1 );
    }

    // Initialize PhysicsFS.
    // We don't have argv[ 0 ], so pass nullptr instead.
    wasPhysFSInitialized = PHYSFS_init( nullptr );
    if( 0 == wasPhysFSInitialized )
    {
        // Nope, PhysicsFS was not initialized. EXIT!
        std::exit( 1 );
    }

    // Initialize PortAudio
    PaError portAudioInit = Pa_Initialize( );
    if( paNoError != portAudioInit )
    {
        // Audio is not initialized. Playable? Nope.
        std::exit( 1 );
    }
    wasPortAudioInitialized = true;
}

#else // ENG_LINUX
// So that there are no compiler/linker complaints about files that do not add anything
// to the final binary.
namespace
{
    int32 ENG_LINUX_ANONYMOUS_INTEGER{ 0 };
}
#endif // ENG_LINUX

