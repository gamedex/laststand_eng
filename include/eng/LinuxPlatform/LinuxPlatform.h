#if !defined( ENG_LINUXPLATFORM_H )
#define ENG_LINUXPLATFORM_H 1

#if defined( __gnu_linux__ )
#define ENG_LINUX 1

struct LinuxTypes : GenericTypes
{
    typedef __SIZE_TYPE__    size;
    typedef __PTRDIFF_TYPE__ ptrdiff;
    typedef char32_t         echar;
};

typedef LinuxTypes PlatformTypes;

#define ENG_PLATFORM_STR( Txt ) ENG_PLATFORM_JOIN( u32, Txt )

#define ENG_IMPORT __attribute__((visibility("default")))
#define ENG_EXPORT __attribute__((visibility("default")))

#else // defined( __gnu_linux__ )
#define ENG_LINUX 0
#endif // defined( __gnu_linux__ )

#endif // !defined( ENG_LINUXPLATFORM_H )

