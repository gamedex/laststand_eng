#if !defined( ENG_GENERICPLATFORM_H )
#define ENG_GENERICPLATFORM_H 1

struct GenericTypes
{
    typedef signed char        int8;
    typedef signed short       int16;
    typedef signed int         int32;
    typedef signed long long   int64;
    typedef unsigned char      uint8;
    typedef unsigned short     uint16;
    typedef unsigned int       uint32;
    typedef unsigned long long uint64;
    typedef float              float32;
    typedef double             float64;
    typedef char               echar;
};

#define ENG_HIDDEN_JOIN( A, B ) A ## B
#define ENG_PLATFORM_JOIN( A, B ) ENG_HIDDEN_JOIN( A, B )

#endif // !defined( ENG_GENERICPLATFORM_H )

