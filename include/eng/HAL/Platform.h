#if !defined( ENG_PLATFORM_H )
#define ENG_PLATFORM_H 1
#include <eng/GenericPlatform/GenericPlatform.h>
#include <eng/LinuxPlatform/LinuxPlatform.h>

// API Import/Export for building and loading dynamic library
//
// Example usage:
// class ENG_API Foo
// {};
#if defined( ENG_BUILD_SHARED )
#define ENG_API ENG_EXPORT
#else // defined( ENG_BUILD_SHARED )
#define ENG_API ENG_IMPORT
#endif // defined( ENG_BUILD_SHARED )

// Join two items together to create a single item.
//
// Example usage:
// wchar_t wideCharacter{ ENG_JOIN( L, 'H' ) };
#define ENG_JOIN( A, B ) ENG_PLATFORM_JOIN( A, B )
// Set string literals to the current type being used by the system.
//
// Example usage:
// eng::String{ ENG_STR( "Hello world!" ) };
#define ENG_STR( Txt ) ENG_PLATFORM_STR( Txt )

// Engine types

typedef PlatformTypes::int8    int8;
typedef PlatformTypes::int16   int16;
typedef PlatformTypes::int32   int32;
typedef PlatformTypes::int64   int64;
typedef PlatformTypes::uint8   uint8;
typedef PlatformTypes::uint16  uint16;
typedef PlatformTypes::uint32  uint32;
typedef PlatformTypes::uint64  uint64;
typedef PlatformTypes::float32 float32;
typedef PlatformTypes::float64 float64;
typedef PlatformTypes::echar   echar;
typedef PlatformTypes::size    size;
typedef PlatformTypes::ptrdiff ptrdiff;


#endif // !defined( ENG_PLATFORM_H )

