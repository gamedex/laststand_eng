#if !defined( ENG_THREADPOOL_H )
#define ENG_THREADPOOL_H 1
#include <eng/HAL/Platform.h>
#include <functional>
#include <utility>

// A thread pool that takes work and performs it asyncrhonously.
class ENG_API ThreadPool
{
public:
    ThreadPool( ) = delete;
    ThreadPool( const ThreadPool& ) = delete;
    ThreadPool( ThreadPool&& ) = delete;
    ~ThreadPool( )
    {}
    ThreadPool& operator=( const ThreadPool& ) = delete;
    ThreadPool& operator=( ThreadPool&& ) = delete;

    // Add work to the queue
    static void AddWork( std::function<void( void )> work );
    // Add work to the queue that takes arguments.
    template<typename... ArgTypes>
    static void AddWork( std::function<void( ArgTypes... )> work, ArgTypes... )
    {
        auto addedWork = [ & ] ( void ) { work( std::forward<ArgTypes>( arguments )... ) };
        AddWork( addedWork );
    }
    // Gets the number of threads the pool is utilizing.
    static uint32 GetThreadCount( );
    // Does the pool have any work to do?
    // Returns true if it does; otherwise, false.
    static bool HasWork( );
    // Join all threads to the main thread. When the threads are done with their current
    // work, they will be destroyed.
    static void JoinThreads( );
};

#endif // !defined( ENG_THREADPOOL_H )

