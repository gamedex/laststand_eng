#/bin/bash

# The directory where this script is located.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# The current working directory.
CWD="$(pwd)"

# Should never build in the source tree.
if [[ -z "${CWD##*$DIR*}" ]]; then
    echo "Error: Do not build in the source directory!"
    exit 1
fi

# Only build Ninja if it does not exist.
if [[ ! -f ninja ]]; then
    # Python is needed to build ninja. Ensure it is in the path.
    PYTHON_PATH=$(which python)
    if [[ ! -x $PYTHON_PATH ]]; then
        echo "Error: python must be in the path to build ninja!"
        exit 1
    fi
    # make the folder to build ninja in
    mkdir ninja_build
    # Go into the ninja_build directory to make ninja
    pushd ninja_build >/dev/null

    # The command to be run to build ninja
    CMD="python ${DIR}/external/ninja/configure.py --bootstrap"
    echo $CMD
    $CMD

    # Copy the produced binary to the build directory.
    cp ninja ../

    # Exit the directory
    popd >/dev/null

    # Then delete the directory used to build ninja
    rm -fr ninja_build
fi

# Make sure the engine build folder exists.
if [[ ! -d engine_build ]]; then
    mkdir engine_build
fi

# Build the engine
pushd engine_build >/dev/null

# The command to generate the files used to build the engine.
CMD="cmake ${DIR} -GNinja -DCMAKE_MAKE_PROGRAM=${CWD}/ninja -DENG_BUILD_TESTS=ON"
echo $CMD
$CMD

# Make sure everything was generated.
if [[ ! -f build.ninja ]]; then
    exit 1
fi

# Build the engine.
${CWD}/ninja

popd >/dev/null
